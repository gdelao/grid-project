// Namespace(s).
GRID = {};
GRID.grid = $("#grid");
GRID.cluster = $(".cluster");
GRID.item = $("li", $(".cluster"));
GRID.nav = $("#grid-nav");
GRID.miniMap = $("#mini-map");
GRID.miniCluster = $("li", $("#mini-map"));
GRID.handle = $("#handle"); // Handle in the mini-map that allows you to drag around the grid.
GRID.miniToFullRatio = parseInt(GRID.miniMap.width()) /  parseInt(GRID.grid.width()); // This helps us determine how much we should move the grid when moving the handle.
GRID.homeBtn = $("#home-btn");
GRID.virtualExhibit = $("#virtual-exhibit-link");
GRID.photoGallery = $("#photo-gallery-link");
GRID.quiz = $("#test-your-knowledge-link");
GRID.slideShowLink = $("#slideshow-link").find("a");
GRID.confirmResize = 0;

$(function(){
	init();
});


function init(){
	
	GRID.getContent();
	GRID.getSidePanelContent();
	GRID.dyk();
	GRID.displayHome();
	GRID.virtEx();
	GRID.home();
	GRID.mmr();
	GRID.navigation();
	GRID.modalHandler();
	GRID.imageGalleryCalc();
	GRID.gallery();
	GRID.quiz();
	GRID.quizBtns();
	GRID.miniMapNav();
	//GRID.previewBox();
	GRID.photoOverlay();
	GRID.helpText();
	GRID.iPadSettings();
	
	GRID.testYourKnowledge();
	GRID.facebookFeed();
	GRID.twitterFeed();
	
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	GRID.isIpad = isiPad;
	
	if(!isiPad){
		$(window).resize(function() {
			if(GRID.confirmResize == 0){
				//if(confirm('For an optimal browsing experience, the browser will be refreshed now.')){
					//window.location.reload();
				//}
				//setTimeout(function(){alert('For an optimal browsing experience, it is recommended that you refresh your web browser after resizing the window.');},3000);
				$('#window-resized').fadeIn('slow');
				
				$('#window-resized').click(function(){
					$(this).fadeOut('slow');
				});
				
				GRID.confirmResize = 1;
			}
		});
		
	}else{
		
		//$("#photo-scroll").hide();
		$(".ipad-minus").css({margin:'-16px 0 0 0!important'});
		$(".ipad-less").css({margin:'4px 0 0 0!important'});  
		//touchScroll("gallery-container");	
	}
	
	
	
	$('#back-to-exhibit').click(function(e){
		e.preventDefault();
		
		$('#virtual-exhibit-link').click();
	});
	
	
	
}

GRID.quizBtns = function(){
	
	$("#btn_ve").click(function(e){
		e.preventDefault();
		$("#virtual-exhibit-link").trigger("click");
	});
	
	$("#btn_pg").click(function(e){
		e.preventDefault();
		$("#photo-gallery-link").trigger("click");
	});
	
}

/*scott added 8/21/2012*/
GRID.testYourKnowledge = function(){
	$('#facebookUpdates').click(function(){
		$('#socialMedia_Twitter').hide();
		$('#socialMedia_Facebook').show();
	});
	
	$('#twitterUpdates').click(function(){
		$('#socialMedia_Facebook').hide();
		$('#socialMedia_Twitter').show();
	});

}


GRID.twitterFeed = function(){
	//idk why, but i had to put this inline to make all the features work from twitter.  check index.html
}

GRID.facebookFeed = function(){
	//just a simple iFrame
}

GRID.iPadSettings = function(){
	
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	
	if(isiPad){
		
		//Hide the minimap.
		$("#mini-map").remove();
		$("#grid-nav").remove();
		
	}
	


		
}


GRID.getContent = function(){
	
	//Grab the content.
	ajaxRequest = null;
	
	ajaxRequest = $.ajax({
		type:'get',
		url:'/samplejson/allClusters.cfm',
		//url:'/data/cluster_subclusters.ashx',
		data: $("").serialize(),
		success:function(response) {							
			GRID.clusterHandler(response);
		},
		error: function(xhr,type,exception) { alert("Error: " + type); },
		complete: function(){
			ajaxRequest = null;
		}
	});
	
}

GRID.helpText = function(){
	
	$("#help-button").click(function(e){
		
		e.preventDefault();
		
		if( $("#help-box").is(':visible') ){
			$("#help-box").fadeOut('swing');
		}else{
			$("#help-box").fadeIn('swing');
		}
		
	});
	
}

GRID.photoOverlay = function(){
	
	$('#preview-box').mouseleave(function(){
		$('#preview-box').hide();
	});
	
	
	$("#close-ae").click(function(e){
		
		e.preventDefault();
		
		$("#ae-content").fadeOut('swing',function(){
			
			// scott added this, lol love the math
			var newWidth = Math.floor($(window).width()/28.3333 - 77);
			$("#american-experience").animate({width: '20px',padding:'65px 0px 0px 0px',left:newWidth+'px'}).addClass('left-round');
			$("#close-ae").fadeOut("fast");
			$("#open-ae").fadeIn("slow",function(){
				
				$(this).click(function(e){
				
					e.preventDefault();
					$("#american-experience").animate({width: '252px',padding:'65px 30px 0px',left:'76px'}).removeClass('left-round');
					$("#ae-content").delay("500").fadeIn("slow");
					$("#open-ae").fadeOut("fast");
					$("#close-ae").fadeIn("fast");
				
				});
			
			});
			
		});
		
	});
	
}

GRID.getSidePanelContent = function(){
	
	ajaxRequest = null;
	
	if(!ajaxRequest){
		
		$.ajax({
			url:'/samplejson/allContents.cfm',
			success:function(response) {							
				GRID.sidePanelHandler(response);
			},
			error: function(xhr,type,exception) { alert("Error: " + type); },
			complete: function(){
				ajaxRequest = null;
			}
		});
		
	}
	
}

GRID.sidePanelHandler = function(data){
	
	var sidePanelTitle = (data[0] !== undefined)?data[0].Title:'';
	var sidePanelContent = (data[0] !== undefined)?data[0].Descript:'';
	$("#panel-title").html(sidePanelTitle);
	$("#panel-content").html(sidePanelContent);
	
	var aeHeader = data[1].Title;
	var aeContent = data[1].Descript;
	$('#ae-content .yellow').html(aeHeader);
	$('#ae-content .white').html(aeContent);		
	
	var footerTitle = (data[2].Title != "")?data[2].Title:'';
	var footerContent = data[2].Descript;
	$('#right .yellow').html(footerContent);
	
	

	
}

GRID.dyk = function(){
	
	$.ajax({
		url:'/samplejson/dyk.cfm',
		success:function(response) {
			
			//Randomize the content. Loop through all of the items, create an array with the id's and randomly select a start position.
			var dykIDs = [];
			var startPos = Math.floor((Math.random()*response.length)+0);
			
			for(i=0; i < response.length; i++ ){
				dykIDs.push(response[i].DYKID);
			}
			
			$("#dyk-title").text(response[startPos].DYKName);
			$("#dyk").text(response[startPos].Descript);
			
			// Grab the next or previous.
			$("#dyk-back").click(function(e){
				
				e.preventDefault();
				
			});
			
			var nextPos = startPos;
			
			$("#dyk-next").click(function(e){
				
				e.preventDefault();
				//Grab the next item.
				nextPos = parseInt(nextPos) + 1;
				if( nextPos > (response.length -1)){
					nextPos = 0;
				}
				$("#dyk-title").text(response[nextPos].DYKName);
				$("#dyk").text(response[nextPos].Descript);
				
			});
			
			$("#dyk-back").click(function(e){
				
				e.preventDefault();
				
				nextPos = parseInt(nextPos) - 1;
				if( nextPos < 0 ){
					nextPos = (response.length - 1);
				}
				$("#dyk-title").text(response[nextPos].DYKName);
				$("#dyk").text(response[nextPos].Descript);
				
			});
			
		},
		error: function(xhr,type,exception) { alert("Error: " + type); },
		complete: function(response){}
	});
	
}

GRID.mmr = function(){
	
	var vEh = 1350;
	var vEw = 2205;//2305;
	
	var VtVpRw = parseInt($(window).width())/parseInt(vEw);
	var VtVpRh = parseInt($(window).height())/parseInt(vEh); 
	
	var e = parseInt($("body").width());
	var ePw = parseInt($("#containment-box").width());
	var ePh = parseInt($("#containment-box").height());
	var z = parseInt(e)/parseInt(ePw);
	
	var cvbw = ePw * VtVpRw;
	var cvbh = ePh * VtVpRh;
	
	//Apply the ratio to the div element.
	$("#current-view-box").css({
		height: cvbh+'px',
		width: cvbw+'px'
	});
	
	GRID.wRatio = z;
	
}

GRID.imageGalleryCalc = function(){
	
	var VPh = parseInt($(window).height());
	var VPw = parseInt($(window).width());
	
	var lPos = (parseInt(VPw)/2) - (620/2);
	var tPos = (parseInt(VPh)/2) - (620/2);
	
	var btnTPos = (parseInt(VPh)/2) - (20/2);
	var btnLPos = ((parseInt(VPw)/2) - (620/2))/2;// - (66/2);
	
	var nbtnLPos = (parseInt(VPw)/2) + (620/2) + ( ((parseInt(VPw)/2) - (620/2))/2 ) - (66/2);// + (66/2);
	
	GRID.prevPos = {
		top: btnTPos,
		left: btnLPos
	};
	
	GRID.nextPos = {
		top: btnTPos,
		left: nbtnLPos
	}
	
}

GRID.clusterHandler = function(data){
	
	var cluster1 = data[0]; //Sub 1-12
	var cluster2 = data[1]; //Sub 13-24
	var cluster3 = data[2];
	var cluster4 = data[3];
	var cluster5 = data[4];
	var cluster6 = data[5];
	var arr = data;
	var subClusterCount = 0;
	
	GRID.mainClusterCount = 6;

	/* 
	What we really want right here is to have the cluster rotate only within itself. 
	So each cluster will have it's own modal handler class. 
	*/
	
	for(var i=0; i<arr.length; i++ ){
		
		//Populate the main cluster item.

		var mainImage = arr[i].ImageFileName;
		
		var industryIndicator = (arr[i].IndustryName != null)?'<div class="industry-info-indicator"></div>':'';
		
		var htmlTemp = '<a href="'+mainImage+'" class="main-modal-link" data-industry-id="'+arr[i].IndustryID+'" data-industry-name="'+arr[i].IndustryName+'" data-industry-desc="'+arr[i].Descript+'" data-cluster="'+i+'"><img src="/image/cluster/'+mainImage+'"/>'+industryIndicator+'</a>';
		$("li.central-box[rel='"+arr[i].ClusterID+"']").addClass('has-image').html(htmlTemp).attr("data-has-image","true").attr("data-industry-desc",arr[i].IndustryDesc).attr("data-industry-name",arr[i].IndustryName);
		
		var subArr = arr[i].SubClusters;

		for(var j=0; j<subArr.length; j++){
			
			var obj = subArr[j];
			var subCluster = obj.SubClusterID;
			var img = obj.ImageFileName;
			var subIndustryIndicator = (subArr[j].IndustryName != null)?'<div class="industry-info-indicator"></div>':'';
			var htmlTemp = '<a href="'+img+'" class="modal-link" data-industry-id="'+subArr[j].IndustryID+'"  data-industry-name="'+subArr[j].IndustryName+'" data-industry-desc="'+subArr[j].IndustryDesc+'" data-cluster="'+i+'"><img src="/image/cluster/thumbnail/'+img+'"/>'+subIndustryIndicator+'</a>';
			
			//Find and populate the content for the sub-cluster.
	        $("li[rel='"+subCluster+"']").addClass("has-image").html(htmlTemp).attr("data-has-image","true");
	        
			subClusterCount = subClusterCount + 1;
		}
	}
	
	GRID.subClusterCount = subClusterCount;
	
	$('.cluster .small-box').css('display', 'none');
	$('.cluster .small-box').find('a').parent().css('display', 'block');
	
}

GRID.home = function(){
	
	GRID.homeBtn.click(function(e){
		
		e.preventDefault();
		
		GRID.displayHome();
		
	});
	
	$("#explore-virtual-exhibit").click(function(e){
		
		e.preventDefault();
		//GRID.virtualExhibit.trigger("click");
		GRID.displayHome();
		
	});
	
	$("#explore-photo-gallery").click(function(e){
		
		e.preventDefault();
		GRID.photoGallery.trigger("click");
		GRID.displayHome();
		
	});
	
	$("#explore-test-knowledge").click(function(e){
		
		e.preventDefault();
		$("#test-your-knowledge").trigger("click");
		GRID.displayHome();
		
	});
	
}

GRID.displayHome = function(){

	if( $("#home").is(':visible') ){
		
		$("#side-panel").fadeOut('slow',function(){
			
			$("#overlay").fadeOut('slow',function(){
				
				$("#home").hide();
				
				$("li",$("#grid-nav")).each(function(){
					$(this).fadeIn('fast');
					
				});
				
			});
			
		});
	
	}else{
			

		$(GRID.virtualExhibit).click();
		$('#photo-gallery-box').fadeOut('fast');
		$('#test-your-knowledge-box').fadeOut('fast');


		$("#home").fadeIn('fast',function(){
			
			$("li",$("#grid-nav")).each(function(){
					$(this).fadeOut('fast');
			});
			
			$("#overlay").fadeIn('fast', function(){
				
				var wH = $(window).height();
				var sPh = parseInt(wH) - 260;
				
				$("#content").css({
					height: sPh+'px'
					});
				
				$("#side-panel").fadeIn('slow');
				
			});
			
		});
		
	}
	
}

GRID.hideHome = function(){
	
	if( $("#home").is(':visible') ){
		
		$("#side-panel").fadeOut('slow',function(){
			
			$("#overlay").fadeOut('slow',function(){
				
				$("#home").hide();
				$("li",$("#grid-nav")).each(function(){
					$(this).fadeIn('fast');
				});
				
			});
			
		});
	
	};
	
}

GRID.navigation = function(){
	
	GRID.arrowsPositioning();
	
	$(window).resize(function(){
		
		GRID.arrowsPositioning();
			
	});
	
	$("#bottom-arrow").click(function(e){
		
		e.preventDefault();
		GRID.scrolling('bottom');
		
	});
	
	$("#top-arrow").click(function(e){
		
		e.preventDefault();
		GRID.scrolling('top');
		
	});
	
	$("#left-arrow").click(function(e){
		
		e.preventDefault();
		GRID.scrolling('left');
		
	});
	
	$("#right-arrow").click(function(e){
		
		e.preventDefault();
		GRID.scrolling('right');
		
	});
	
	$('#overlay').click(function(){
		GRID.displayHome();
	});
}

GRID.scrolling = function(direction){
	
	if( direction == 'top' ){
		var scrollPos = window.pageYOffset - 300;
	}else if( direction == 'bottom' ){
		var scrollPos = window.pageYOffset + 300;
	}else if( direction == 'left' ){
		var scrollPos = window.pageXOffset - 300;
	}else if( direction == 'right'){
		var scrollPos = window.pageXOffset + 300;
	}
	
	if( scrollPos != GRID.oldScrollPos ){
		
	GRID.oldScrollPos = scrollPos;	
		
		if( direction == 'top' || direction == 'bottom' ){
			
			$('html, body').animate({
				scrollTop: scrollPos//$(item).offset().top
			}, 800);
			var mmTop = scrollPos/15;
			if( mmTop <= 56 && mmTop >= 0){
				$("#current-view-box").css({top:mmTop+'px'});	
			}else{
				if( mmTop > 56 ){
					$("#current-view-box").css({top:'56px'});	
				}else{
					$("#current-view-box").css({top:'0px'});
				}
				
			}
		}else{
			$('html, body').animate({
				scrollLeft: scrollPos
			}, 800);
			var mmLeft = scrollPos/GRID.wRatio
			if( mmLeft <= 64 && mmLeft >= 0){
				$("#current-view-box").css({left:mmLeft+'px'});
			}else{
				if( mmLeft >= 64 ){
					$("#current-view-box").css({left:'64px'});	
				}else{
					$("#current-view-box").css({left:'0px'});
				}
				
			}
		}
	
	}
	
}

GRID.arrowsPositioning = function(){
	
	/* Position the arrows. */
	var top = $("#top-arrow");
	var bottom = $("#bottom-arrow");
	var left = $("#left-arrow");
	var right = $("#right-arrow");
	var winWidth = $(window).width();
	var winHeigth = $(window).height();
	var middle = (parseInt(winWidth) - 17)/2;
	var vMiddle = (parseInt(winHeigth) - 17)/2;
	
	top.css({
		left: middle+'px'
	});
	
	bottom.css({
		left: middle+'px'
	});
	
	left.css({
		top: vMiddle+'px'
	});
	
	right.css({
		top: vMiddle+'px'
	});
	
}

GRID.miniMapNav = function(){
	
	$("#current-view-box").draggable({ 
		containment: "#containment-box", 
		scroll: false,
		start: function(event, ui){
			
		},
		stop: function(event, ui){
			
			// Move the grid.
			var scrollPosTop = ui.position.top * 20;
			var scrollPosLeft = ui.position.left * GRID.wRatio;
			
			$('html, body').animate({
				scrollTop: scrollPosTop,
				scrollLeft: scrollPosLeft
			}, 100);
			
		}
	});
	
}

GRID.modal = function(img){
	
	var winWidth = $(window).width();
	var winHeigth = $(window).height();
	var mWidth = 620;
	var mHeigth = 620;
	var mid = (winHeigth - mHeigth)/2;
	var vmid = (winWidth - mWidth)/2;
	var nbMid = (winHeigth - 240)/2;
	
	var VPh = parseInt($(window).height());
	var VPw = parseInt($(window).width());
	var btnTPos = (parseInt(VPh)/2) - (60/2);
	var btnLPos = vmid - 97;//((parseInt(VPw)/2) - (620/3))/2;// - (66/2);
	
	var nbtnLPos = vmid + 650;//(parseInt(VPw)/2) + (620/3) + ( ((parseInt(VPw)/2) - (620/3))/3 ) - (66/3);
	
	$("#go-back").css({
		top: btnTPos,
		left: btnLPos
	});
	
	$("#next").css(GRID.nextPos = {
		top: btnTPos,
		left: nbtnLPos
	});
	
	
	GRID.modalImage(img);
	
	$("#modal").fadeIn('fast');
	$("#modal").find($("#container")).css({
		top: mid+'px',
		left: vmid+'px',
		position: 'absolute'
	}).fadeIn('fast',function(){
		
		$("#close").click(function(e){
			e.preventDefault();
			$('#grid-nav').show();
			$("#modal").fadeOut('fast',function(){
				$("#overlay").fadeOut('fast',function(){
					$("#home").hide();
				});
				
				$("#container").removeClass("mainitem");
			});
		});
		
	});
	
}

GRID.modalImage = function(img){
	
	var imgPath = '/image/cluster/'+img;
	$("#modal-pic").hide().attr("src",imgPath).fadeIn('fast');
	
}

GRID.modalHandler = function(){
	
	$("body").on("click",".main-modal-link,.modal-link",function(e){
		
		e.preventDefault();
		
		var currentImg = $(this).attr("href");
		var clusterID = $(this).data("cluster");
		var tabTitle = $(this).data("industry-name");
		var tabContent = $(this).data("industry-desc");
		var industryid = $(this).data("industry-id");
		
		//Get the number of items in the cluster.
		var cluster = $(this).parent("li").parent("ul");
		var iArr = [];
		var tabContentArr = [];
		
		cluster.children().each(function(){
			if( $(this).data("has-image") ){
				var img = $(this).children("a").attr("href");
				iArr.push(img);
				
				var tabTitl = $(this).children("a").data("industry-name");
				var tabDesc = $(this).children("a").data("industry-desc");
				var industryid = $(this).children("a").data("industry-id");
				var tmpJSON = {title:tabTitl, desc:tabDesc, industryid:industryid};
				tabContentArr.push(tmpJSON);
				
			}
			
			var tabTitle = $(this).data("industry-name");
			var tabContent = $(this).data("industry-desc");
			var industryid = $(this).data("industry-id");
		});
				
		var arrPos = $.inArray(currentImg,iArr);
		var arrMax = iArr.length - 1;
		
		if(1==1 || arrPos == 0){
			
			if( tabTitle != null ){
				$("#container").addClass("mainitem");
				$("#tab-title").text(tabTitle);
				$("#tab-title").removeClass();
				$("#tab-title").addClass('industry' + industryid);
				$("#tab-content").text(tabContent);
			}
			
		}else{
			$("#container").removeClass("mainitem");
		}
		
		//Populate with clicked image.
		$("#home").fadeIn('fast',function(){
			
			$("#overlay").fadeIn('fast',function(){
				GRID.modal(currentImg);
				$('#grid-nav').hide();
			});
			
		});	
		
		//Next and Previous modal nav buttons.
		$("body").on("click","#modal-next",function(e){
			
			e.preventDefault();
			
			arrPos = arrPos + 1;
			if(arrPos > arrMax){
				arrPos = 0;
			}
			
			tabTitle = tabContentArr[arrPos].title;
			tabContent = tabContentArr[arrPos].desc;
			industryid = tabContentArr[arrPos].industryid;
			if( tabTitle != null ){				
				$("#container").addClass("mainitem");
				$("#tab-title").text(tabTitle);
				$("#tab-title").removeClass();
				$("#tab-title").addClass('industry' + industryid);				
				$("#tab-content").text(tabContent);
			}else{
				$("#container").removeClass("mainitem");
			}
				
			GRID.modalImage(iArr[arrPos]);
			
			
		});
		
		
		$("body").on("click","#modal-back",function(e){
			
			e.preventDefault();
			
			arrPos = arrPos - 1;
			if(arrPos < 0){
				arrPos = arrMax;
			}
			
			tabTitle = tabContentArr[arrPos].title;
			tabContent = tabContentArr[arrPos].desc;
			industryid = tabContentArr[arrPos].industryid;
			
			if( tabTitle != null ){				
				$("#container").addClass("mainitem");
				$("#tab-title").text(tabTitle);
				$("#tab-title").removeClass();
				$("#tab-title").addClass('industry' + industryid);				
				$("#tab-content").text(tabContent);
			}else{
				$("#container").removeClass("mainitem");
			}
			
			GRID.modalImage(iArr[arrPos]);
			
			
		});
		
		
		$("#tab").click(function(){
		
			if( parseInt($(this).css("height")) > 52 ){
				$(this).animate({"height":"52px"},200);	
			}else{
				$(this).animate({"height":"290px"},200);
			}
			
		});
		
	});
	
	
}

GRID.previewBox = function(){
	
	var previewLink = $(".preview-link");
	
	
	var hovered = false;
	
	
	previewLink.click(function(e){
	
	
	//i butchered this beautiful code.  if iPad, show the lightbox without showing the middle step.
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	if(isiPad)
	{
		var imgName = $(this).attr("href");
		var imageCaption = $(this).attr('rel');
		$("#slide-image-standalone").attr("src",imgName);
		$("#slide-caption-standalone").text(imageCaption);	
		
		
		$('#photo-slideshow-standalone').show();
		var l = (parseInt($(window).width())/2) - 361;
	
		$("#photo-slideshow-standalone").fadeIn("fast",function(){
			
			$("#slide-overlay-standalone").fadeIn("fast",function(){
					
				$("#slide-container-standalone").css({
					left: l+'px'
				}).fadeIn('fast');		
					
				$('#close-slideshow-standalone').click(function(){
					$("#photo-slideshow-standalone").fadeOut("fast");
				});	
					
			}).click(function(){
				
				
				$("#photo-slideshow-standalone").fadeOut("fast");
				
			});
			
		});
			
			return;
	}
	//end scotts butchering



	
	
	
		
		//scott added this -- if closed, don't show the hover items...probably a better way to do this.
		if($('#american-experience').width() != '20'){
			return;
		}
			
		e.preventDefault();
		
		var cITop = $(this).position().top -22;
		var cILeft = $(this).position().left - 100;
		cfILeft = (cILeft <= '-2.6')?10:cILeft;
		
		var imgName = $(this).attr("href");
		var cap = $(this).attr("rel");
		var imageTag = '<img src="'+imgName+'" height="194" item-id="'+ $(this).attr("item-id") +'"/>';
		$("#preview-image").html(imageTag);
		$("#caption").text(cap);
		
		$("#preview-box").css({
			top: cITop+'px',
			left: cILeft+'px'
		}).fadeIn("fast",function(){
			
			$(this).unbind('click').click(function(e){
				e.preventDefault();
				$(this).fadeOut("fast");
				
				
				//scott
				
				var itemSrc = $(this).find('img').attr('src');
				var imageCaption = $(this).find('#caption').text();
				$("#slide-image-standalone").attr("src",itemSrc);
				$("#slide-caption-standalone").text(imageCaption);	
				
				
				$('#photo-slideshow-standalone').show();
			
			
				var l = (parseInt($(window).width())/2) - 361;
			
				$("#photo-slideshow-standalone").fadeIn("fast",function(){
					
					$("#slide-overlay-standalone").fadeIn("fast",function(){
							
						$("#slide-container-standalone").css({
							left: l+'px'
						}).fadeIn('fast');		
							
						$('#close-slideshow-standalone').click(function(){
							$("#photo-slideshow-standalone").fadeOut("fast");
						});	
							
					}).click(function(){
						
						$("#photo-slideshow-standalone").fadeOut("fast");
						
					});
					
				});
			
			});
			
		});
		
	}).mouseenter(function(){
		
		var eL = $(this);
		
		$(this).trigger("click");
		
	}).mouseleave(function(){
		
		hovered = false;
	});
	
		
	return;
	
	previewLink.each(function(){
							
		$(this).click(function(e){
			
			e.preventDefault();
			
			var cITop = $(this).position().top -52;
			var cILeft = $(this).position().left - 100;
			cfILeft = (cILeft <= '-2.6')?10:cILeft;
			
			var imgName = $(this).attr("href");
			var cap = $(this).attr("rel");

			var imageTag = '<img src="/image/photo/'+imgName+'" height="194"/>';
			$("#preview-image").html(imageTag);
			
			$("#preview-box").css({
				top: cITop+'px',
				left: cILeft+'px'
			}).fadeIn("fast",function(){
				
				$(this).click(function(e){
					e.preventDefault();
					$(this).fadeOut("fast");
				});
				
			});
			
			return;
			
		});
			
	});
		
}

/* Virtual Exhibit. */
GRID.virtEx = function(){
	
	GRID.virtualExhibit.click(function(e){
		
		e.preventDefault();
		
		$("#test-your-knowledge-box").fadeOut("fast");
		$("#photo-gallery-box").fadeOut("fast");
		
		$("#virtual-exhibit").fadeIn("fast",function(){
			$("#mini-map").fadeIn('fast');
			$("#help-button").fadeIn('fast');
		});
		
	});

	  
}

/* Gallery. */
GRID.gallery = function(){
	
	GRID.photoGallery.click(function(e){
		
		e.preventDefault();
		
		// Make the call to the photo-gallery.
		ajaxRequest = null;
	
		ajaxRequest = $.ajax({
			type:'get',
			url:'/samplejson/allPhotos.cfm',
			success:function(response) {							
				
				// Populate the image gallery.
				$("#photo-thumbs").html("");
				
				var arr = response;
				
				for(var i=0; i<arr.length; i++ ){
					
					var img = arr[i].ServerFileName;
					var caption = arr[i].Caption;
					
					var htmlTemp = '<li><a class="preview-link" item-id="'+i+'" href="/image/photo/'+img+'" rel="'+caption+'" ><img src="/image/photo/thumbnail/'+img+'" /></a></li>';
					
					$("#photo-thumbs").append(htmlTemp);
					
				}
				
				$("#photo-thumbs").append('<div class="clear"></div>');
				
				GRID.hideHome();
				$("#test-your-knowledge-box").fadeOut("fast");
				$("#virtual-exhibit").fadeOut("fast",function(){
					
					$("#mini-map").fadeOut('fast');
					$("#help-box").fadeOut('fast');
					$("#help-button").fadeOut('fast');
					var w = (parseInt($(window).width())/8);
					//scott added this line above
					
					$("#photo-gallery-box").css({
						left: w+'px'
					}).
					fadeIn("fast",function(){
						
						//Preview box.
						GRID.previewBox();
						
						GRID.photoGalleryHeight = $("#photo-thumbs").height();
						GRID.photoRatio = parseInt(GRID.photoGalleryHeight)/360;
						
						if( GRID.photoGalleryHeight < 360 ){
							$("#photo-scroll").hide();
						}else{
							if(!GRID.isIpad){
								$("#photo-scroll").fadeIn('fast');
							}else{
								$("#photo-scroll").fadeIn('fast');
							}
						}
												
					});
					
				});
				
			},
			error: function(xhr,type,exception) { alert("Error: " + type); },
			complete: function(){
				ajaxRequest = null;
			}
		});
		
	});
	
	//Image scroll bar.
	$("#scroll-btn").draggable({
		axis: "y", 
		containment: "#photo-scroll", 
		scroll: false,
		start: function(event, ui){
			
		},
		drag: function(event, ui){
			
			var sPos = ui.position.top * GRID.photoRatio;
			$("#gallery-container").scrollTop(sPos);
		},
		stop: function(event, ui){}
	});
	
	
	GRID.slideShowLink.click(function(e){
		
		e.preventDefault();
		
		// Grab all the photos.
		ajaxRequest = null;
	
		ajaxRequest = $.ajax({
			type:'get',
			url:'/samplejson/allPhotos.cfm',
			success:function(response) {
				
				var arr = response;
				var counter = 0;
				
				//Grab the image at counter.
				$("#slide-image").attr("src",'/image/photo/'+arr[counter].ServerFileName);
				
				$("#slide-right").click(function(e){
					
					e.preventDefault();
					counter = ( (counter +1) <= (arr.length-1) )? counter + 1: 1;
					
					$("#slide-image").attr("src",'/image/photo/'+arr[counter].ServerFileName);
					$("#slide-caption").text(arr[counter].Caption);
						
				});
				
				$("#slide-left").click(function(e){
					
					e.preventDefault();
					counter = ( (counter - 1) >= 0)? counter-1:0;
					
					$("#slide-image").attr("src",'/image/photo/'+arr[counter].ServerFileName);
					$("#slide-caption").text(arr[counter].Caption);
					
				});
				
				
				autoRotate = setInterval(function(){
					$("#slide-right").trigger("click");
				}, 5000);
				
				$("#pause").click(function(e){
					
					e.preventDefault();
					
					if( $("#pause-link").text() != "resume" ){
						
						autoRotate = clearInterval(autoRotate);
						$("#pause-link").text("resume");
						$("#pause-link").parent().addClass("paused");
					
					}else{
						
						$("#pause-link").text("pause");
						$("#pause-link").parent().removeClass();
						
						autoRotate = setInterval(function(){
							$("#slide-right").trigger("click");
						}, 3500);	
						
					}
					
				});
				
				
			},
			complete: function(){
				ajaxRequest = null;
			}
		});	
		

							
		var l = (parseInt($(window).width())/2) - 361;
		
			$("#photo-slideshow").fadeIn("fast",function(){
				
				$("#slide-overlay").fadeIn("fast",function(){
						
					$("#slide-container").css({
						left: l+'px'
					}).fadeIn('fast');		
						
					$('#close-slideshow').click(function(){
						$("#photo-slideshow").fadeOut("fast");
					});	
						
				}).click(function(){
					
					$("#photo-slideshow").fadeOut("fast");
					
				});
				
			});
		
	});
	
}

GRID.quiz = function(){
	
	$("#test-your-knowledge").click(function(e){
		
		e.preventDefault();

		$("#mini-map").fadeOut('fast');
		$("#help-box").fadeOut('fast');
		$("#help-button").fadeOut('fast');
		
		ajaxRequest = null;
	
		ajaxRequest = $.ajax({
			type:'get',
			url:'/samplejson/allQuizes.cfm',
			success:function(response) {
				
				var qArr = response;
				var qCount = qArr.length;
				var alpha = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
				var currentQuestion = 0;
				var correctCount = 0;
				var cQuestion = currentQuestion + 1;
				var cAns = qArr[currentQuestion].AID;
				var question = qArr[currentQuestion].QuizQuestionText;
				var answersArr = qArr[currentQuestion].QuizAnswers;
				//Keep a track of the answers. 
				var userAnswersArr = "";
				
				$(".question-count").text("Question "+cQuestion+"/"+qArr.length);
				
				$(".question").text(question);
				
				$("#answer-selection").html("");
				
				for(var i=0; i<answersArr.length; i++ ){
					
					var aHtmlTemp = '<p class="answers"><input type="radio" name="'+currentQuestion+'" value="'+answersArr[i].QuizAnswerID+'" id="'+answersArr[i].QuizAnswerID+'"/><label for="'+answersArr[i].QuizAnswerID+'"><strong>'+alpha[i]+'.</strong> '+answersArr[i].QuizAnswerText+'</label></p>';
					
					$("#answer-selection").append(aHtmlTemp);
					
				}
				
				$("#quiz").find("#next").click(function(e){
					
					e.preventDefault();
					
					//First, validate the answer.
					if (!$("#quiz").find(":checked").length){
						alert("Please select an answer.");
						return;
					}
					
					//Increment qCount, and save the answer.
					if( $("input[id='"+cAns+"']").is(":checked")){
						
						//They got this one right, count it.
						correctCount = correctCount + 1;
						
						// Grab the next question.
						currentQuestion = currentQuestion + 1;
						userAnswersString = 'q'+currentQuestion+'Answer='+cAns;
						//userAnswersArr.push(userAnswersString);
						
						userAnswersArr = (userAnswersArr.length > 0)?userAnswersArr + "&" + userAnswersString: userAnswersString;
						
						if(currentQuestion < qCount){
						
							cQuestion = currentQuestion + 1;
							cAns = qArr[currentQuestion].AID;
							question = qArr[currentQuestion].QuizQuestionText;
							answersArr = qArr[currentQuestion].QuizAnswers;
							
							GRID.grabQuestion(currentQuestion,cQuestion,question,answersArr,alpha,qCount);
						
						}else{
							
							//Tally the last score.
							$("#quiz").fadeOut("fast",function(){
								
								$("#score").text(correctCount);
								$("#total-questions").text(qCount);
								$("#quiz-results").fadeIn("fast");
								
							});
							
							var quizURL = 'http://bach.corporatezen.com:50055/data/QuizSubmit.ashx?firstname=Scott&lastname=Buckel&email=scott@corporatezen.com&' + userAnswersArr;
							
							//Post the answers.
							$.ajax({
								type: 'POST',
								url: quizURL,
								success:function(response){
									$("#percent-return").text(response);
								},
								complete:function(){}
							});
							
							
						}
								
					}else{ //They got it wrong.
						
						$("#quiz").find(":checked").next("label").css({color:'red'});
						
							$("#quiz").find("#next").fadeOut('swing',function(){
							
								//alert("Sorry, the correct answer is" + " " + $("input[id='"+cAns+"']").next("label").text());
								$("#wrong-answer-feedback").text("Sorry, the correct answer is" + " " + $("input[id='"+cAns+"']").next("label").text()).fadeIn("swing",function(){
								
									$("#quiz").find("#continue").fadeIn('slow',function(){
										
										//gotta unbind click, each time you click continue it rebinds it, makes continue button click several times.
										$(this).unbind('click').click(function(e){
											
											e.preventDefault();
											
											currentQuestion = currentQuestion + 1;
											userAnswersString = 'q'+currentQuestion+'Answer='+$("#quiz").find(":checked").val();
											userAnswersArr = (userAnswersArr.length > 0)?userAnswersArr + "&" + userAnswersString: userAnswersString;
											//userAnswersArr.push(userAnswersString);
											//console.log(userAnswersArr);
											if(currentQuestion < qCount){
											
												cQuestion = currentQuestion + 1;
												cAns = qArr[currentQuestion].AID;
												question = qArr[currentQuestion].QuizQuestionText;
												answersArr = qArr[currentQuestion].QuizAnswers;
												
												GRID.grabQuestion(currentQuestion,cQuestion,question,answersArr,alpha,qCount);
											
											}else{
												
												//Tally the last score.
												$("#quiz").fadeOut("fast",function(){
													
													$("#score").text(correctCount);
													$("#total-questions").text(qCount);
													$("#quiz-results").fadeIn("fast");
													
												});
												
												var quizURL = 'http://bach.corporatezen.com:50055/data/QuizSubmit.ashx?firstname=Scott&lastname=Buckel&email=scott@corporatezen.com&' + userAnswersArr;
							
												//Post the answers.
												$.ajax({
													type: 'POST',
													url: quizURL,
													success:function(response){
														$("#percent-return").text(response);
													},
													complete:function(){}
												});
												
											}
											
											$("#wrong-answer-feedback").text("");
											
											$(this).fadeOut('slow',function(){
												$("#quiz").find("#next").fadeIn('slow');	
											});
											
										});
											
									});
									
							});
						
						});
						
					}
					
				});
				
			},
			completed:function(){
				
				ajaxRequest = null;
				
			}
		});
		
		GRID.hideHome();
		$("#virtual-exhibit,#photo-gallery-box").fadeOut("fast",function(){
			
			$("#mini-map").fadeOut('fast');
			var w = (parseInt($(window).width())/2) - 442;
			
			$("#test-your-knowledge-box").css({
				left: w+'px'
			});
			
			$("#test-your-knowledge-box").fadeIn("fast",function(){
				
			});
			
		});
		
	});
	
}

GRID.grabQuestion = function(currentQuestion,cQuestion,question,answersArr,alpha,qCount){
	
	$(".question-count").text("Question "+cQuestion+"/"+qCount);
				
	$(".question").text(question);
	
	$("#answer-selection").html("");
	
	for(var i=0; i<answersArr.length; i++ ){
					
		var aHtmlTemp = '<p class="answers"><input type="radio" name="'+currentQuestion+'" value="'+answersArr[i].QuizAnswerID+'" id="'+answersArr[i].QuizAnswerID+'"/><label for="'+answersArr[i].QuizAnswerID+'"><strong>'+alpha[i]+'.</strong> '+answersArr[i].QuizAnswerText+'</label></p>';
		
		$("#answer-selection").append(aHtmlTemp);
		
	}
	
}


function isTouchDevice(){
	try{
		document.createEvent("TouchEvent");
		return true;
	}catch(e){
		return false;
	}
}

function touchScroll(id){
	if(isTouchDevice()){ //if touch events exist...
		var el=document.getElementById(id);
		var scrollStartPos=0;

		document.getElementById(id).addEventListener("touchstart", function(event) {
			scrollStartPos=this.scrollTop+event.touches[0].pageY;
			event.preventDefault();
		},false);
		
		document.getElementById(id).addEventListener("touchmove", function(event) {
			this.scrollTop=scrollStartPos-event.touches[0].pageY;
			event.preventDefault();
		},false);
		
	}
}

