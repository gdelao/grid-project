<!DOCTYPE HTML>
<html>
<head>
<title>Zurich Virtual Exhibit</title>

<meta name="viewport" content="width=device-width, initial-scale=0.9 maximum-scale=.9, user-scalable=no" />

<link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

<link rel="stylesheet" href="css/reset.css" media="all" />
<link rel="stylesheet" href="css/main.css" media="all" />
<!-- <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" /> -->

<!--[if lte IE 8]>
<style>
	#grid #header{
		border-bottom: 3px solid #000;
	}
<![endif]-->
</style>
</head>

<body>
	<div id="window-resized">
		<p>For an optimal browsing experience, it is recommended that you refresh your web browser after resizing the window.</p>
	</div>
	
	<div id="modal">
		<ul id="modal-nav">
			<li id="go-back"><a id="modal-back" href="">Back</a></li>
			<li id="next"><a id="modal-next" href="">Next</a></li>
		</ul>
		<div id="container">
			<a id="close">X</a>
			<img src="/images/sample_image.png" id="modal-pic"/>
			<div id="tab">
				<div id="tab-title">Title</div>
				<div id="tab-content">Content</div>
			</div>
		</div>
	</div>
	
	<div id="home">
		<div id="side-panel">
			<div id="header">
				<a href="http://www.zurichna.com"><div id="logo"></div></a>
			</div>	
			<div id="content">
				<h1 id="panel-title">Placeholder</h1>
				
				<p id="panel-content">
					Placeholder
				</p>
				
				<ul id="explorer-nav">
					<li><a id="explore-virtual-exhibit" href="">Explore the Virtual Exhibit</a></li>
					<li><a id="explore-photo-gallery" href="">Visit the Exhibit Photo Gallery</a></li>
					<li><a id="explore-test-knowledge" href="">Test Your Knowledge</a></li>
				</ul>
				
			</div>
			<div id="footer">
		
				<ul id="social-media">
					<a href=""><li id="cloud"></li></a>
					<a href=""><li id="facebook"></li></a>
					<a href="">
						<li id="twitter"></li>
					</a>			
				</ul>

			</div>
		</div>
				
		<div id="overlay"></div>
	
	</div>
	
	<div id="grid">
		
		<div id="header">
			<a href="http://www.zurichna.com"><div id="logo"></div></a>
			<ul id="nav-tabs">
				<li><a id="virtual-exhibit-link" href="">Virtual Exhibit</a></li>
				<li><a id="photo-gallery-link" href="">Exhibit Photo Gallery</a></li>
				<li><a id="test-your-knowledge" class="no-border" href="">Test Your Knowledge</a></li>
			</ul>
			<a href="" id="home-btn">Home</a>
			<div id="mini-map">
				<div id="containment-box">
					<div id="current-view-box"></div>
				</div>	
			</div>
		</div>
		
		<a id="help-button">HELP</a>
		<div id="help-box">
			<div id="tips">NAVIGATION TIPS</div>
			<ul id="instructions">
				<li>To move around and see more content, click the directional arrows on the map.</li> 
				<li>Click any panel to enlarge. Return to the map by clicking on the X button.</li>
				<li>Use the menu bar at the top of the screen to visit other areas on the site.</li>
			</ul>
		</div>
		
		<div id="virtual-exhibit">
			
			<div id="lines"></div>
			
			<ul id="grid-nav">
				<li id="top-arrow"><a href="">Top</a></li>
				<li id="right-arrow"><a href="">Right</a></li>
				<li id="bottom-arrow"><a href="">Bottom</a></li>
				<li id="left-arrow"><a href="">Left</a></li>
			</ul>
			
			
			
			<ul class="cluster" id="cluster1">
				<li class="central-box" rel="1" data-cluster="1" id="box0">
					<img src="/images/clusters/1.png"/>
				</li>
				<li class="small-box" id="box1" rel="1" data-cluster="1"></li>
				<li class="small-box" id="box2" rel="2" data-cluster="1"></li>
				<li class="small-box" id="box3" rel="3" data-cluster="1"></li>
				<li class="small-box" id="box4" rel="4" data-cluster="1"></li>
				<li class="small-box" id="box5" rel="5" data-cluster="1"></li>
				<li class="small-box" id="box6" rel="6" data-cluster="1"></li>
				<li class="small-box" id="box7" rel="7" data-cluster="1"></li>
				<li class="small-box" id="box8" rel="8" data-cluster="1"></li>
				<li class="small-box" id="box9" rel="9" data-cluster="1"></li>
				<li class="small-box" id="box10" rel="10" data-cluster="1"></li>
				<li class="small-box" id="box11" rel="11" data-cluster="1"></li>
				<li class="small-box" id="box12" rel="12" data-cluster="1"></li>
			</ul>
			
			
			<ul class="cluster" id="cluster2">
				<li class="central-box" rel="2" data-cluster="2" id="box0">
					
				</li>
				<li class="small-box" id="box1" rel="13" data-cluster="2"></li>
				<li class="small-box" id="box2" rel="14" data-cluster="2"></li>
				<li class="small-box" id="box3" rel="15" data-cluster="2"></li>
				<li class="small-box" id="box4" rel="16" data-cluster="2"></li>
				<li class="small-box" id="box5" rel="17" data-cluster="2"></li>
				<li class="small-box" id="box6" rel="18" data-cluster="2"></li>
				<li class="small-box" id="box7" rel="19" data-cluster="2"></li>
				<li class="small-box" id="box8" rel="20" data-cluster="2"></li>
				<li class="small-box" id="box9" rel="21" data-cluster="2"></li>
				<li class="small-box" id="box10" rel="22" data-cluster="2"></li>
				<li class="small-box" id="box11" rel="23" data-cluster="2"></li>
				<li class="small-box" id="box12" rel="24" data-cluster="2"></li>
			</ul>
			
			<ul class="cluster" id="cluster3">
				<li class="central-box" rel="3" data-cluster="3" id="box0">
					
				</li>
				<li class="small-box" id="box1" rel="25" data-cluster="3"></li>
				<li class="small-box" id="box2" rel="26" data-cluster="3"></li>
				<li class="small-box" id="box3" rel="27" data-cluster="3"></li>
				<li class="small-box" id="box4" rel="28" data-cluster="3"></li>
				<li class="small-box" id="box5" rel="29" data-cluster="3"></li>
				<li class="small-box" id="box6" rel="30" data-cluster="3"></li>
				<li class="small-box" id="box7" rel="31" data-cluster="3"></li>
				<li class="small-box" id="box8" rel="32" data-cluster="3"></li>
				<li class="small-box" id="box9" rel="33" data-cluster="3"></li>
				<li class="small-box" id="box10" rel="34" data-cluster="3"></li>
				<li class="small-box" id="box11" rel="35" data-cluster="3"></li>
				<li class="small-box" id="box12" rel="36" data-cluster="3"></li>
			</ul>
			
			<ul class="cluster" id="cluster4">
				<li class="central-box" rel="4" data-cluster="4" id="box0">
					
				</li>
				<li class="small-box" id="box1" rel="37" data-cluster="4"></li>
				<li class="small-box" id="box2" rel="38" data-cluster="4"></li>
				<li class="small-box" id="box3" rel="39" data-cluster="4"></li>
				<li class="small-box" id="box4" rel="40" data-cluster="4"></li>
				<li class="small-box" id="box5" rel="41" data-cluster="4"></li>
				<li class="small-box" id="box6" rel="42" data-cluster="4"></li>
				<li class="small-box" id="box7" rel="43" data-cluster="4"></li>
				<li class="small-box" id="box8" rel="44" data-cluster="4"></li>
				<li class="small-box" id="box9" rel="45" data-cluster="4"></li>
				<li class="small-box" id="box10" rel="46" data-cluster="4"></li>
				<li class="small-box" id="box11" rel="47" data-cluster="4"></li>
				<li class="small-box" id="box12" rel="48" data-cluster="4"></li>
			</ul>
			
			<ul class="cluster" id="cluster5">
				<li class="central-box" rel="5" data-cluster="5" id="box0">
					
				</li>
				<li class="small-box" id="box1" rel="49" data-cluster="5"></li>
				<li class="small-box" id="box2" rel="50" data-cluster="5"></li>
				<li class="small-box" id="box3" rel="51" data-cluster="5"></li>
				<li class="small-box" id="box4" rel="52" data-cluster="5"></li>
				<li class="small-box" id="box5" rel="53" data-cluster="5"></li>
				<li class="small-box" id="box6" rel="54" data-cluster="5"></li>
				<li class="small-box" id="box7" rel="55" data-cluster="5"></li>
				<li class="small-box" id="box8" rel="56" data-cluster="5"></li>
				<li class="small-box" id="box9" rel="57" data-cluster="5"></li>
				<li class="small-box" id="box10" rel="58" data-cluster="5"></li>
				<li class="small-box" id="box11" rel="59" data-cluster="5"></li>
				<li class="small-box" id="box12" rel="60" data-cluster="5"></li>
			</ul>
			
			
			<ul class="cluster" id="cluster6">
				<li class="central-box" rel="6" data-cluster="6" id="box0">
				</li>
				<li class="small-box" id="box1" rel="61" data-cluster="6"></li>
				<li class="small-box" id="box2" rel="62" data-cluster="6"></li>
				<li class="small-box" id="box3" rel="63" data-cluster="6"></li>
				<li class="small-box" id="box4" rel="64" data-cluster="6"></li>
				<li class="small-box" id="box5" rel="65" data-cluster="6"></li>
				<li class="small-box" id="box6" rel="66" data-cluster="6"></li>
				<li class="small-box" id="box7" rel="67" data-cluster="6"></li>
				<li class="small-box" id="box8" rel="68" data-cluster="6"></li>
				<li class="small-box" id="box9" rel="69" data-cluster="6"></li>
				<li class="small-box" id="box10" rel="70" data-cluster="6"></li>
				<li class="small-box" id="box11" rel="71" data-cluster="6"></li>
				<li class="small-box" id="box12" rel="72" data-cluster="6"></li>
			</ul>
			
		
		</div>
		
		<!--- Photo Gallery. --->
		<div id="photo-gallery-box">
			<div id="photos-container">
				
				<div id="american-experience">
					<a id="close-ae"></a>
					<a id="open-ae"></a>
					<div id="ae-content">
					<h2 class="yellow">Our American Experiences</h2>
					
					<p class="white">
						Placeholder
					</p>
					</div>
					
				</div>
				
				<ul id="photo-gallery-nav">
					<li id="slideshow-link"><a href="">Slideshow</a></li>
				</ul>
				
				<div class="clear"></div>
				
				<div id="gallery-container">
					<div id="scroll-cover"></div>			
					<ul id="photo-thumbs" class="image-content"></ul>
				</div>
				
				<div id="photo-scroll">
					<div id="scroll-btn"></div>
				</div>
				
				<div id="preview-box">
					<div id="preview-image"></div>
					<div id="caption">Caption truncated as necessary.</div>
				</div>
				
			</div>
			
		</div>
		
		<!--Photo slide show. -->
		<div id="photo-slideshow">
				
			<div id="slide-container">
			<a id="close-slideshow">X</a>
				<a id="slide-left">Back</a>
				<div id="image-holder">
					<img src="" id="slide-image"/>
				</div>
				<div id="slide-caption">
					Image caption...
				</div>
				<a id="slide-right">Next</a>
				<div id="pause"><a id="pause-link" href="*">Pause</a></div>
			</div>
			
			<div id="slide-overlay"></div>
			
		</div>
		
		
		<!--Photo slide show. -->
		<div id="photo-slideshow-standalone">
				
			<div id="slide-container-standalone">
			<a id="close-slideshow-standalone">X</a>
				<div id="image-holder-standalone">
					<img src="" id="slide-image-standalone"/>
				</div>
				<div id="slide-caption-standalone">
					Image caption...
				</div>
	
			</div>
			
			<div id="slide-overlay-standalone"></div>
			
		</div>
		
		<!-- Test your knowledge. -->
		<div id="test-your-knowledge-box">
			
			<div id="quiz-container">
			
				<div id="quiz-knowledge">
					
					<div id="quiz">
					
						<h2>Test Your Knowledge</h2>
						<p class="question-count">Question</p>
						
						<p class="question">
							
						</p>
						
						<div id="answer-selection">
						
						</div>
						
						<div id="wrong-answer-feedback">
							
						</div>
						
						<a href="" id="next">Next</a>
						<a href="" id="continue">Continue</a>
					
					</div>
					
					<div id="quiz-results">
						
						<h1>Thank you for taking Zurich's <br/>Test Your Knowledge Quiz!</h1>
						
						<p>
							Your score is <span class="yellow" id="score">9</span> correct out of <span id="total-questions">10</span> questions.
						</p>
						
						<a id="back-to-exhibit" href="#">Back to Virtual Exhibit</a>
						
					</div>
					
				</div>
				
				<div id="social-media-box">
					
					<ul id="face_twitter">
						<li id="facebook">
							<a href="#" id="facebookUpdates">FACEBOOK UPDATES</a>
						</li>
						<li id="twitter">
							<a href="#" id="twitterUpdates">FOLLOW ON TWITTER</a>
						</li>
					</ul>
				

					
					<div id="socialMedia_Facebook">
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FHelpPointAdvocate%3FWT.mc_id%3Dsmfacebook&amp;width=305&amp;height=357&amp;colorscheme=light&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=true&amp;" scrolling="no" frameborder="0" style="margin-top:10px;border:none; overflow:hidden; width:305px; height:357px;" allowTransparency="true"></iframe>
					</div>
					
					<div id="socialMedia_Twitter">

					
					<p style="margin-left:20px;padding-top:25px;"><a class="twitter-follow-button" data-lang="en" data-show-count="true" data-show-screen-name="true" href="https://twitter.com/ZurichNAnews">Follow @ZurichNAnews</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></p>
					
					
					<hr class="title-line"/>
						
						
					<div id="twitter_div" style="height:317px;overflow-y:auto;">
						<ul id="twitter_update_list">
							<li style="text-align: center;">
								<p style="margin-top: 50px;">
									<span style="padding: 0 20px;">Loading tweets...</span></p>
							</li>
						</ul>
					</div>
						
					



	
				</div>
					
					
					<!--
					<hr class="title-line"/>
						<hr class="gray"/>
					<h3>
						Zurich Heritage on Facebook
					</h3>					
					<p class="update">
						Raymond Moore just scored 8/10 on Zurich�s Test Your Knowledge quiz.<br/>
						
						<span class="gray">Tuesday at 8:33am</span>
					</p>
					
					<p class="update">
						Raymond Moore just scored 8/10 on Zurich�s Test Your Knowledge quiz.<br/>
						
						<span class="gray">Tuesday at 8:33am</span>
					</p>
					
					<hr class="gray"/>
					
					
					<p class="update">
						Raymond Moore just scored 8/10 on Zurich�s Test Your Knowledge quiz.<br/>
						
						<span class="gray">Tuesday at 8:33am</span>
					</p>
					
					<hr class="gray"/>
					-->
					
				</div>
			
			</div>
			
		</div>
		
		<div id="footer">
			<div id="bottom-social">
				<ul id="social-media">
					<a href=""><li id="cloud"></li></a>
					<a href=""><li id="facebook"></li></a>
					<a href=""><li id="twitter"></li></a>
				</ul>
				<img src="/images/zurichnorthamerica.png"> <br/>
				<a href="http://www.zurichna.com/zna/services/termsofuse.htm" target="_blank">
					<img src="/images/zurichtermsofservice.png">
				</a>
			</div>
			
			
			
			<div id="bottom-content">
				<div id="left">

					<div id="right">
						<h3 class="yellow"></h3>
					</div>
					
					
					<ul id="dyk-nav">
						<li><a href="" id="dyk-back">back</a></li>
						<span id="pipe">|</span>
						<li><a href="" id="dyk-next">next</a></li>
					</ul>
					<h3 class="yellow" id="dyk-title"></h3>
					<p id="dyk">
						
					</p>
					

				</div>

			</div>
			
		</div>
		
		
	</div>

<script type="text/javascript" src="/javascripts/jquery.min.js"></script>
<script type="text/javascript" src="/javascripts/jquery-ui-1.8.22.custom.min.js"></script>
<!--
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script src="/javascripts/jquery.mousewheel.min.js"></script>
 custom scrollbars plugin 
<script src="/javascripts/jquery.mCustomScrollbar.js"></script>
-->

<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/ZurichNAnews.json?callback=twitterCallback2&count=8"></script>


<script type="text/javascript" src="/javascripts/main.js"></script>

</body>

</html> 